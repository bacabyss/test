﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidSolution
{
    [Activity(Label = "SecondActivity")]
    public class SecondActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here

            SetContentView(Resource.Layout.SecondActivity);

            View contenView = FindViewById(Resource.Id.secondScreen);
            contenView.SetBackgroundColor(Android.Graphics.Color.Purple);

            Button SecondButton = (Button)FindViewById(Resource.Id.SecondButton);
            SecondButton.Click += delegate { StartActivity(typeof(MainActivity));};
        }
    }
}
