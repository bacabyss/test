﻿using System;
using System.Collections.Generic;
using RelevantCodes.ExtentReports;
using UITest.Common;
using UITest.TestScreens;

namespace UITest.Utils
{
    public static class Factory
    {

        static Dictionary<string, Type> dTestScreen = new Dictionary<string, Type>(){
            {nameof(DummyScreen1),typeof(DummyScreen1)},
            {nameof(DummyScreen2),typeof(DummyScreen2)}
        };

        public static BaseTestScreen CreateTestScreen(String screenName,BaseTest baseTest,ExtentReports report)
        {
            return (BaseTestScreen)(dTestScreen.ContainsKey(screenName) ? Activator.CreateInstance(dTestScreen[screenName],baseTest,report) : throw new NotSupportedException());
        }
    }
}
