﻿using System;
using System.IO;
using NUnit.Framework;
using RelevantCodes.ExtentReports;
using Xamarin.UITest;

namespace UITest.Utils
{
    public static class CommonUtils
    {
        public static ExtentTest ExtentTest;
        public static string CaptureImage(string screenName,IApp app)
        {
            // var timestamp = DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss-fff");
           
            var imagePath = Constants.TestImagePath  + screenName + ".png";
            if (File.Exists(imagePath))
            {
                File.Delete(imagePath);
            }
            app.Screenshot("Screenshot").MoveTo(imagePath);

            return imagePath;
        }

        public static void VerifyScreen(string screenName,IApp app,ExtentReports report)
        {
            var imagePath = CaptureImage(screenName,app);
            ExtentTest = report.StartTest(screenName);
            ExtentTest.Log(LogStatus.Info,"Screen Verified");
        }

    }
}
