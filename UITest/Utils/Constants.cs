﻿using System;
namespace UITest.Utils
{
    public class Constants
    {
        /// <summary>
        /// The xml node name.
        /// </summary>
        public const string FlowXmlNodeList = "TestFlows";
        public const string ScreenXmlNode = "Screen";
        public const string ScreenIDXmlNode = "ScreenName";
        public const string ScreenListXmlNode = "ScreenList";
        public const string ActionXmlNode = "Actions";

        public const string TestReportPath = "../../TestReport/";
        public const string TestImagePath = "../../CaptureImage/";


        public static class Action
        {
            public const string VerifyScreen = "VerifyScreen";
            public const string AccessToDummy2 = "AccessToDummy2";
        }
    }
}
