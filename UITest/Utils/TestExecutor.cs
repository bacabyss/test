﻿using System;
using System.Collections.Generic;
using System.Xml;
using NUnit.Framework;
using UITest.Common;
using RelevantCodes.ExtentReports;

namespace UITest.Utils
{ 
    [TestFixture]
    public class TestExecutor : BaseTest
    {
        ExtentReports Report;
        [Test]
        public void TestExecute()
        {
            ExecuteTestScreen("VerifyScreen");
        }

        public void ExecuteTestScreen(string testCase)
        {
            var list = GetListTestScreen(testCase);
            foreach (BaseTestScreen testScreen in list)
            {
                foreach (string action in testScreen.Actions)
                {
                    testScreen.ExecuteAction(action);
                }
            }
            Report.EndTest(CommonUtils.ExtentTest);
            Report.Flush();
        }

        private static XmlNode GetXmlFlowScreen(String testCase)
        {
            var document = new XmlDocument();
            XmlNode result = null;
            document.LoadXml(Properties.Resources.TestFlows);
            XmlNodeList nScreenFlowList = document.SelectNodes(Constants.FlowXmlNodeList);
            foreach (XmlNode node in nScreenFlowList)
            {
                var innerNodes = node.SelectNodes(Constants.ScreenListXmlNode);
                foreach (XmlNode innernode in innerNodes)
                {
                    foreach (XmlAttribute att in innernode.Attributes)
                    {
                        if (att.Value.Equals(testCase))
                        {
                            result = innernode;
                        }
                    }
                }
            }
            return result;
        }

        private List<BaseTestScreen> GetListTestScreen(string testcase)
        {
            var screenFlowNode = GetXmlFlowScreen(testcase);
            List<BaseTestScreen> listTestScreen = new List<BaseTestScreen>();
           
            Report = new ExtentReports(Constants.TestReportPath + testcase + ".html",DisplayOrder.NewestFirst);
            var screenNodes = screenFlowNode.SelectNodes(Constants.ScreenXmlNode);
            foreach (XmlNode screenNode in screenNodes)
            {
                var screenTest = Factory.CreateTestScreen(screenNode[Constants.ScreenIDXmlNode].InnerText,this,Report);
                if (screenTest != null)
                {
                    var actionNodes = screenNode[Constants.ActionXmlNode];
                    foreach (XmlNode action in actionNodes)
                    {
                        screenTest.Actions.Add(action.InnerText);
                    }
                    listTestScreen.Add(screenTest);
                }
            }
            return listTestScreen;
        }
    }
}
