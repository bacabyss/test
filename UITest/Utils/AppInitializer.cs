﻿using System;
using Xamarin.UITest;

namespace UITest.Common
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {

            return ConfigureApp.Android
                // TODO: Update this path to point to your Android app and uncomment the
                // code if the app is not included in the solution
                .InstalledApp("fso.fsu2.z79.AndroidSolution")
                .WaitTimes(new WaitTimes())
                .Debug()
                .EnableLocalScreenshots()
                .StartApp();
        }

        public class WaitTimes : Xamarin.UITest.Utils.IWaitTimes
        {
            public TimeSpan GestureWaitTimeout { get { return TimeSpan.FromMinutes(2); } }
            public TimeSpan WaitForTimeout { get { return TimeSpan.FromMinutes(2); } }
            public TimeSpan GestureCompletionTimeout { get { return TimeSpan.FromMinutes(2); } }
        }
    }
}
