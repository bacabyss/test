﻿using System;
using RelevantCodes.ExtentReports;
using UITest.Common;
using UITest.Utils;

namespace UITest.TestScreens
{
    public class DummyScreen2 : BaseTestScreen
    {
        private ExtentReports Report;
        public DummyScreen2(BaseTest baseTest, ExtentReports report) : base(baseTest)
        {
            Report = report;
        }

        public override void ExecuteAction(string action)
        {
            switch (action)
            {
                case Constants.Action.VerifyScreen:
                    CommonUtils.VerifyScreen(nameof(DummyScreen1), App, Report);
                    break;                
            }
        }

    }
}
