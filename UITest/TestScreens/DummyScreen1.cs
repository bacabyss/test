﻿using System;
using RelevantCodes.ExtentReports;
using UITest.Common;
using UITest.Utils;

namespace UITest.TestScreens
{
    public class DummyScreen1 : BaseTestScreen
    {
        private ExtentReports Report;

        public DummyScreen1(BaseTest baseTest, ExtentReports report) : base(baseTest)
        {
            Report = report;
        }

        public override void ExecuteAction(string action)
        {
            switch (action)
            {
                case Constants.Action.VerifyScreen:
                    CommonUtils.VerifyScreen(nameof(DummyScreen2), App, Report);
                    break;
                case Constants.Action.AccessToDummy2:
                    App.Tap(c => c.Button("FirstScreen"));    
                    break;
            }
        }


    }
}
