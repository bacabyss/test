﻿using NUnit.Framework;
using Xamarin.UITest;

namespace UITest.Common
{
   
    public class BaseTest
    {
        public IApp App;

        [SetUp]
        public void BeforeEachTest()
        {
            //Starting test
            App = AppInitializer.StartApp(Platform.Android);
           
        }

        [TearDown]
        public void TearDown()
        {
           //Execute before end testcase
        }
    }

}
