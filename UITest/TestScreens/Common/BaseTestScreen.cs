﻿using System;
using System.Collections.Generic;
using UITest.Utils;
using Xamarin.UITest;

namespace UITest.Common
{
    public abstract class BaseTestScreen
    {
        protected IApp App;
        public List<string> Actions { get; set; } = new List<string>();
        public abstract void ExecuteAction(string action);

        public BaseTestScreen(BaseTest baseTest) { App = baseTest.App; }
    }
}
